/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "glwidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionExit;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    GLWidget *panelGL;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QSpacerItem *verticalSpacer;
    QMenuBar *menuBar;
    QMenu *menuArchivo;
    QMenu *menuAyuda;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::ApplicationModal);
        MainWindow->resize(796, 457);
        QIcon icon;
        icon.addFile(QStringLiteral("../../skull-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        panelGL = new GLWidget(centralWidget);
        panelGL->setObjectName(QStringLiteral("panelGL"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(panelGL->sizePolicy().hasHeightForWidth());
        panelGL->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(panelGL);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setStyleSheet(QStringLiteral(""));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_2);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setStyleSheet(QStringLiteral(""));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_4);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_5);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_6);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_7);

        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_8);

        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_9);

        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_10);

        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_11);

        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_12);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 796, 19));
        menuArchivo = new QMenu(menuBar);
        menuArchivo->setObjectName(QStringLiteral("menuArchivo"));
        menuAyuda = new QMenu(menuBar);
        menuAyuda->setObjectName(QStringLiteral("menuAyuda"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        menuBar->addAction(menuArchivo->menuAction());
        menuBar->addAction(menuAyuda->menuAction());
        menuArchivo->addAction(actionExit);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Sistema Oseo - Cabeza", nullptr));
        actionExit->setText(QApplication::translate("MainWindow", "&Exit", nullptr));
#ifndef QT_NO_SHORTCUT
        actionExit->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        label_2->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Es un hueso del Cr\303\241neo, plano, par, de forma cuadril\303\241tera, con dos caras, interna (endocraneal) y externa (exocraneal), y cuatro bordes con sus respectivos \303\241ngulos.</p><p>Se encuentra cubriendo la porci\303\263n superior y lateral del cr\303\241neo, por detr\303\241s del Frontal, por delante del Occipital y montada sobre el Temporal y el Esfenoides.</p><p>Ambos huesos parietales se articulan, a trav\303\251s de una l\303\255nea media: la sutura sagital.</p><p>El t\303\251rmino parietal significa de la <span style=\" font-weight:600; font-style:italic; text-decoration: underline;\">pared</span><span style=\" font-weight:600;\">.</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("MainWindow", " Parietal", nullptr));
#ifndef QT_NO_TOOLTIP
        label_3->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>El hueso frontal (os frontale PNA) es un hueso del cr\303\241neo, plano, impar, central y sim\303\251trico, con dos caras (endocraneal y exocraneal) y un borde circunferencial.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("MainWindow", "Frontal", nullptr));
#ifndef QT_NO_TOOLTIP
        label->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Es un hueso impar, central y sim\303\251trico, que ocupa la parte anterior y media de la base de la cavidad \303\263sea.</p><p>Est\303\241 situado entre el etmoides y el frontal, que se encuentran por delante, y el occipital, que est\303\241 por detr\303\241s.</p><p>Al primer golpe de vista, el esfenoides aparece como un hueso muy complejo.</p><p>En conjunto, el esfenoides se compone de un cuerpo de forma c\303\272bica, dos alas menores anexas a la parte superior del cuerpo, dos alas mayores anexas a las caras laterales del cuerpo, dos ap\303\263fisis pterigoides a modo de tren de aterrizaje y dos ganchos, por detr\303\241s de estas.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("MainWindow", "Esfenoides", nullptr));
#ifndef QT_NO_TOOLTIP
        label_4->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Forma parte del suelo de la fosa craneal anterior y participa en el macizo facial (cavidad nasal y \303\263rbitas).</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("MainWindow", "Etmoides", nullptr));
#ifndef QT_NO_TOOLTIP
        label_5->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Dos huesos peque\303\261os y rectangulares que var\303\255an en tama\303\261o.</p><p>Se encuentran uno al lado del otro entre los procesos frontales de los huesos maxilares y se unen para formar el puente de la nariz.</p><p>Estos huesos sirven de inserci\303\263n a los tejidos cartilaginosos que son los principales responsables de la forma de la nariz.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("MainWindow", "Nasal", nullptr));
#ifndef QT_NO_TOOLTIP
        label_6->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Peque\303\261o hueso plano, par, que se encuentra formando la pared interna de la \303\263rbita ocular, cerca de los huesos nasales, en la parte posterior y lateral de estos, hueso fino y fr\303\241gil.</p><p>Tambi\303\251n se conoce como <span style=\" font-weight:600; font-style:italic; text-decoration: underline;\">Unguis.</span></p><p><br/></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_6->setText(QApplication::translate("MainWindow", "Lagrimal", nullptr));
#ifndef QT_NO_TOOLTIP
        label_7->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Es un hueso del Cr\303\241neo, par, de forma irregular cuadril\303\241tera.</p><p>Es el hueso m\303\241s importante del Viscerocr\303\241neo.</p><p>En su interior se encuentra una cavidad, recubierta de mucosa y rellena de aire, denominada Seno maxilar.</p><p>Se sit\303\272a en la cara, por encima de la cavidad bucal, por debajo de la cavidad orbitaria y lateralmente a las cavidades nasales, formando parte de las tres cavidades.</p><p>Se articula con el del lado contrario y presenta una gran cavidad en su interior: el seno maxilar.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_7->setText(QApplication::translate("MainWindow", "Maxilar", nullptr));
#ifndef QT_NO_TOOLTIP
        label_8->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Es un elemento funcional bilateral que act\303\272a equilibrando el cr\303\241neo sobre la columna cervical, la relaci\303\263n muscular entre las tres estructuras es muy estrecha y como sucede en el resto del cuerpo, todo guarda relaci\303\263n.</p><p>Nada funciona sin solicitar ayuda de zonas anexas, una lesi\303\263n cervical puede afectar a la mec\303\241nica de la ATM y viceversa.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_8->setText(QApplication::translate("MainWindow", "Mand\303\255bula", nullptr));
#ifndef QT_NO_TOOLTIP
        label_9->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Es un hueso del cr\303\241neo, par, irregular, de estructura compleja, que comprende tres funciones del esqueleto, y no solo forma parte de la pared lateral y la base de cr\303\241neo, sino que en su interior contiene los \303\263rganos de la audici\303\263n y del equilibrio.</p><p>El temporal es producto de la fusi\303\263n de varios huesos (huesos mixtos), compuesto por tres partes e independientes en varios animales.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_9->setText(QApplication::translate("MainWindow", "Temporal", nullptr));
#ifndef QT_NO_TOOLTIP
        label_10->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Es un hueso par, corto y compacto, situado en la parte m\303\241s externa de la cara, en forma cuadril\303\241tera que forma el p\303\263mulo de la cara y parte de la \303\263rbita y presentan un saliente o proceso cigom\303\241tico que se une hacia atr\303\241s con el proceso cigom\303\241tico del hueso temporal.</p><p>Se articulan con el temporal, el maxilar, esfenoides y el frontal.</p><p>Son responsables de las prominencias de las mejillas por debajo y a los lados de los ojos.</p><p>Estos huesos tambi\303\251n ayudan a formar las paredes laterales y el suelo de las \303\263rbitas (ojo).</p><p>Cada hueso tiene un proceso <span style=\" font-style:italic;\">&quot;temporal&quot;,</span> que se extiende por la parte posterior para sumarse al proceso cigom\303\241tico del hueso temporal.</p><p>Juntos, estos procesos forman un <span style=\" font-weight:600; font-style:italic; text-decoration: underline;\">&quot;arco cigom\303\241tico&quot;</span>. La prominencia lisa entre las cejas se le "
                        "llama <span style=\" font-weight:600; font-style:italic; text-decoration: underline;\">&quot;glabela&quot;</span>.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_10->setText(QApplication::translate("MainWindow", "Cigom\303\241tico", nullptr));
#ifndef QT_NO_TOOLTIP
        label_11->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Se une a los huesos parietales a lo largo de la sutura &quot;lamboidea&quot; y constituye la parte posterior y la base del cr\303\241neo, tiene una gran abertura en su superficie inferior llamada Foramen m\303\241gnum, a trav\303\251s de la cual las fibras nerviosas del cerebro pasan y entran en el canal vertebral para formar parte de la m\303\251dula espinal. Procesos redondeados llamados &quot;c\303\263ndilos occipitales&quot; que se encuentran a cada lado del foramen magnum, se unen con la primera v\303\251rtebra de la columna vertebral. La uni\303\263n de las suturas, sagital y lamboidea, se denomina <span style=\" font-style:italic;\">Lambda</span>.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_11->setText(QApplication::translate("MainWindow", "Occipital", nullptr));
#ifndef QT_NO_TOOLTIP
        label_12->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Es un hueso largo y plano que se une con el hueso etmoides para crear el tabique nasal.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_12->setText(QApplication::translate("MainWindow", "V\303\263mer", nullptr));
        menuArchivo->setTitle(QApplication::translate("MainWindow", "&Archivo", nullptr));
        menuAyuda->setTitle(QApplication::translate("MainWindow", "A&yuda", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
