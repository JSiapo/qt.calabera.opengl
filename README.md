# Craneo en Qt usando OpenGL
Para el dibujo del cráneo se usó Blender siendo exportado el objeto en formato .obj del cual se usó las coordenadas en la graficación para OpenGL
<center>
![imagen](https://gitlab.com/JSiapo/qt.calabera.opengl/raw/master/Images/demo.gif)
</center>

## Dependencias

OpenGL

```bash
sudo apt install freeglut3 freeglut3-dev
```

