#include "glwidget.h"
#include "opengl.h"

GLWidget::GLWidget(QWidget *parent):
    QGLWidget(parent)
{
    connect(&timer,SIGNAL(timeout()),this,SLOT(updateGL()));
    timer.start(16);
}

void GLWidget::initializeGL(){
    glClearColor(0.95,0.95,0.95,1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
}

void GLWidget::paintGL(){
    dibuja_craneo();
}

void GLWidget::resizeGL(int w,int h){
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,(float)w/h,0.01,100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0,0,5, 0,0,0 ,0,1,0);
}
